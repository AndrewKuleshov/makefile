iEdit: main.o ./Editor/editor.o ./TextLine/text_line.o

    g++ main.o ./Editor/editor.o ./TextLine/text_line.o -o $@

%.o: %.cpp

    gcc -c $<

main.o: main.cpp ./Editor/editor.h ./TextLine/text_line.h main.h
editor.o: ./Editor/editor.cpp ./Editor/editor.h
text_line.o: ./TextLine/text_line.cpp ./TextLine/text_line.h
